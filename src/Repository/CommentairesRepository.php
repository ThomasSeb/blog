<?php

namespace App\Repository;

use App\Entity\Commentaires;


class CommentairesRepository {

    public function add(Commentaires $commentaire): void
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO commentaires (commentaires_pseudo, commentaires_date, commentaires_contenu, id_article) 
                                        VALUES (:commentaires_pseudo, NOW(), :commentaires_contenu, :id_article)");
        $query->bindValue(":commentaires_pseudo", $commentaire->commentaires_pseudo, \PDO::PARAM_STR);
        // $query->bindValue(":commentaires_date", $commentaire->commentaires_date->format('Y-m-d'));
        $query->bindValue(":commentaires_contenu", $commentaire->commentaires_contenu, \PDO::PARAM_STR);
        $query->bindValue(":id_article", $commentaire->id_article, \PDO::PARAM_INT);
        $query->execute();

        $commentaire->commentaires_id = $connection->lastInsertId();
        

    }

    public function findAll(int $id)
    {

        $commentaires = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT *
        FROM commentaires WHERE commentaires.id_article = :id ORDER BY commentaires_id DESC");
        $query->bindValue(":id", $id, \PDO::PARAM_INT);
        $query->execute();

        foreach ($query->fetchAll() as $line) {
            $commentaires[] = $this->sqlToCommentaire($line);
        }
        return $commentaires;
        
    }

    private function sqlToCommentaire(array $line): Commentaires {
        $commentaire = new Commentaires();

        $commentaire->commentaires_id = intval($line["commentaires_id"]);
        $commentaire->commentaires_pseudo = $line["commentaires_pseudo"];
        $commentaire->commentaires_date = new \DateTime($line["commentaires_date"]);
        $commentaire->commentaires_contenu = $line["commentaires_contenu"];
        $commentaire->id_article = intval($line["id_article"]);

        return $commentaire;
    }
}