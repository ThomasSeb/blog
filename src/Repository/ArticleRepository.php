<?php

namespace App\Repository;

use App\Entity\Article;
use Symfony\Component\VarDumper\Server\Connection;


class ArticleRepository {

    public function findAll()
    {
        $articles=[];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article");
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            
            $articles[] = $this->sqlToArticle($line);
        }

        return $articles;
    }

    public function findLastRows():array
    {
        $articles = [];

        $connection = ConnectionUtil::getConnection();

        $query = $connection->prepare("SELECT * FROM article order by id_article desc limit 8");

        $query->execute();

        
        foreach ($query->fetchAll() as $line) {
            $articles[] = $this->sqlToArticle($line);
        }
        return $articles;
    }

    public function add(Article $article): void
    {
        dump($article);
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("INSERT INTO article (titre_article, auteur_article, date_article, contenu_article, url_image) 
                                        VALUES (:titre_article, :auteur_article, NOW(), :contenu_article, :url_image)");
        $query->bindValue(":titre_article", $article->titre_article, \PDO::PARAM_STR);
        $query->bindValue(":auteur_article", $article->auteur_article, \PDO::PARAM_STR);
        // $query->bindValue(":date_article", $article->date_article->format('Y-m-d'));
        $query->bindValue(":contenu_article", $article->contenu_article, \PDO::PARAM_STR);
        $query->bindValue(":url_image", $article->url_image, \PDO::PARAM_STR);
        $query->execute();

        $article->id_article = $connection->lastInsertId();

    }

    public function find(int $id): ? Article
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article WHERE id_article=:id_article");
        $query->bindValue(":id_article", $id, \PDO::PARAM_INT);
        $query->execute();

        if ($line = $query->fetch()) {

            return $this->sqlToArticle($line);
        }

        return null;
        
    }

    public function lastId(): Article
    {
        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article ORDER BY id_article DESC LIMIT 1");
        $query->execute();

        if ($line = $query->fetch()) {

            return $this->sqlToArticle($line);
        }
    }

    public function update(Article $article): void
    {
       $connection = ConnectionUtil::getConnection();
       $query = $connection->prepare("UPDATE article SET titre_article = :titre_article, auteur_article = :auteur_article, date_article = NOW(), contenu_article = :contenu_article, url_image = :url_image WHERE id_article = :id_article");
       $query->bindValue(":titre_article", $article->titre_article);
       $query->bindValue(":auteur_article", $article->auteur_article);
    //    $query->bindValue(":date_article", $article->date_article->format('Y-m-d'));
       $query->bindValue(":contenu_article", $article->contenu_article);
       $query->bindValue(":url_image", $article->url_image);
       $query->bindValue(":id_article", $article->id_article);
       $query->execute();
    }

    public function remove(Article $article): void
    {
       $connection = ConnectionUtil::getConnection();
       $query2 = $connection->prepare(" DELETE FROM commentaires WHERE id_article = :id_article");
       $query2->bindValue(":id_article", $article->id_article);
       $query2->execute();
       $query = $connection->prepare("DELETE FROM article WHERE id_article = :id_article");
       $query->bindValue(":id_article", $article->id_article);
       $query->execute();

       

    
    }

    public function search(String $words)
    {

        $article = [];

        $connection = ConnectionUtil::getConnection();
        $query = $connection->prepare("SELECT * FROM article WHERE titre_article LIKE :words");
        $query->bindValue(":words", '%'.$words.'%');
        $query->execute();

        foreach ($query->fetchAll() as $key => $value) {
            $article[]= $this->sqlToArticle($value);
        }
        return $article;
    }


    private function sqlToArticle(array $line): Article {
        $article = new Article();

        $article->id_article = intval($line["id_article"]);
        $article->titre_article = $line["titre_article"];
        $article->auteur_article = $line["auteur_article"];
        $article->date_article = new \DateTime($line["date_article"]);
        $article->contenu_article = $line["contenu_article"];
        $article->url_image = $line["url_image"];

        return $article;
    }
}