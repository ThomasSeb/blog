<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\UrlType;


class ArticleType extends AbstractType {

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre_article', TextType::class)
            ->add('auteur_article', TextType::class)
            // ->add('date_article', DateType::class, [
            //     'widget' => 'single_text', 'format' => 'yyyy-MM-dd'])

            ->add('url_image', UrlType::class)
            ->add('contenu_article', TextareaType::class)
            ;
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data-class'=>Article::class
        ]);
    }
    
    
}