<?php

namespace App\Entity;

class Article {

    public $id_article;
    public $titre_article;
    public $auteur_article;
    public $date_article;
    public $contenu_article;
    public $url_image;
}