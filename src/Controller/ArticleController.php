<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Commentaires;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;


class ArticleController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function admin()
    {
        return $this->redirectToRoute("all_articles");
    }

    /**
     * @Route("/ajouter-article", name="ajout")
     */
    public function articleForm(Request $request, ArticleRepository $repo)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->add($article);
            return $this->redirectToRoute('accueil');
        }
        return $this->render('add-article.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @Route("/", name="accueil")
     */
    public function accueil(ArticleRepository $repo)
    {

        
        return $this->render('accueil.html.twig',[
            "articles"=>$repo->findLastRows(),
            "lastArticle"=>$repo->lastId()
        ]);
    }

    /**
     * @Route("/all-articles", name="all_articles")
     */
    public function showArticles(ArticleRepository $repo)
    {

        
        return $this->render('all-articles.html.twig',[
            "articles"=>$repo->findAll(),
            "lastArticle"=>$repo->lastId()
        ]);
    }

    /**
     * @Route("/article/{id}", name="show_article")
     */
    public function oneArticle(int $id, ArticleRepository $repo, CommentairesRepository $comRepo, Request $request)
    {
        $commentaires = new Commentaires();
        $commentaires->id_article = $id;
        $form = $this->createForm(CommentairesType::class, $commentaires);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comRepo->add($commentaires);
            return $this->redirectToRoute('show_article', array('id' => $id));
        }
        return $this->render('show-article.html.twig',[
            "formCom" => $form->createView(),
            "article"=>$repo->find($id),
            "commentaires" =>$comRepo->findAll($id)
        ]);
    }

    /**
     * @Route("/admin/delete-article/{id}", name="delete_article")
     */
    public function deleteArticle(int $id, ArticleRepository $repo)
    {
        $article = $repo->find($id);
        $deleteArticle = $repo->remove($article);
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        return $this->redirectToRoute("accueil");
    }

    /**
     * @Route("/admin/edit-article/{id}", name="edit_article")
     */

    public function edit(int $id, Request $request, ArticleRepository $repo)
    {
        $oldEdit = $repo->find($id);
        $form = $this->createForm(ArticleType::class, $oldEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($oldEdit);
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
            return $this->redirectToRoute('show_article', [
                'id'=>$id
            ]);
        }
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');
        return $this->render("edit-article.html.twig", [
            "form"=>$form->createView()
        ]);
    }

    /**
     * @Route("/search-article", name="search_article")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        
        $articles = $repo->search($request->get('search'));
        return $this->render('search-articles.html.twig',[
            "articles" => $articles
        ]);

    }

    /**
     * @Route("/a-propos", name="a_propos")
     */
    public function aPropos()
    {
        return $this->render('a-propos.html.twig');
    }

    
}

