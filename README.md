# Blog

Création d'un blog à l'aide de symfony et PDO.

## Technologies utilisées

PHP Symfony
PDO
Bootstrap

## Diagrammes UML

Diagramme usecase qui permet de définir les fonctionnalités du site:
<br>
<img src = "/ressources/usecase.png">

Diagramme de classes:
<br>
<img src = "/ressources/classdiagram.png">

## Fonctionnalités

Sur la page d'accueil le dernier article est affiché en entier, puis les articles sont affichés en petites cartes avec leur contenu réduit et la possibilité de voir l'article complet avec un bouton lire plus.

Possibilités d'ajouter des articles, de les commenter et si vous êtes connecté en tant qu'admin de les modifier et les supprimer.

Il est aussi possible d'effectuer une recherche parmi les titres des articles.
Par exemple, si on met "art" dans la recherche tous les articles ayant "art" quelque part dans leur titre seront affichés.



## License

Ce projet est totalement libre d'utilisation.


