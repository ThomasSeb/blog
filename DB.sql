-- MySQL dump 10.17  Distrib 10.3.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	10.3.13-MariaDB-1:10.3.13+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `titre_article` varchar(100) DEFAULT NULL,
  `auteur_article` varchar(45) DEFAULT NULL,
  `date_article` date DEFAULT NULL,
  `contenu_article` text DEFAULT NULL,
  `url_image` text DEFAULT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'mon premier article','Moi','2019-03-13','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices sed metus vel blandit. Nunc eget sem non turpis tincidunt tincidunt. Maecenas auctor gravida eros at aliquet. Praesent et varius odio, in scelerisque arcu. Vivamus sagittis enim et quam tincidunt dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean convallis, risus sed porttitor lacinia, quam arcu porttitor risus, ut sollicitudin risus neque sit amet nibh. Curabitur a commodo augue. Phasellus ac imperdiet justo. Sed fringilla posuere nibh, sed porttitor quam finibus mattis. Curabitur finibus varius lacus. Etiam consectetur id mi eget aliquet. Quisque placerat porttitor imperdiet.\r\n\r\nMaecenas molestie aliquet consequat. In gravida sapien et nibh maximus consectetur. Vivamus ac massa sit amet dui congue tincidunt in ac libero. Nunc ornare eleifend velit. Vestibulum molestie justo turpis, quis luctus ligula imperdiet eu. Duis aliquam eu ex sit amet luctus. In lobortis dapibus massa. Nullam at egestas sapien. Morbi vel ante nisl. Duis suscipit id ex in gravida. Ut nec laoreet libero, eget lobortis erat. Cras ultricies iaculis tortor, ut tincidunt massa aliquam pretium. Donec pulvinar, justo vitae tristique convallis, nulla odio maximus nisi, vel posuere nulla nisi laoreet velit.','https://images.unsplash.com/photo-1531959870249-9f9b729efcf4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=726&q=80'),(2,'Mon deuxieme article','Re moi','2019-03-13','Ut pellentesque lorem velit. Sed at aliquam nibh, condimentum ornare dolor. Curabitur sed est sed neque vehicula porta. Mauris ac posuere velit. Duis gravida cursus efficitur. Phasellus quis vestibulum dolor. Vivamus sem arcu, euismod ac diam eu, blandit ultrices est. Praesent tincidunt condimentum dolor eu vehicula.\r\n\r\nUt sollicitudin, enim molestie fermentum ultrices, nulla magna imperdiet felis, vitae tincidunt est est in velit. In finibus sem nec eros pulvinar malesuada. Donec mi lectus, placerat at elementum dictum, tincidunt et purus. Quisque ultrices massa quis elit mollis tincidunt. Etiam eu blandit risus. Nam ac tempus ipsum. Donec id semper quam. Sed faucibus orci nulla, a scelerisque diam pellentesque eu.','https://images.unsplash.com/photo-1520848315518-b991dd16a625?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80'),(4,'troisieme article modif','RE re moi modif','2019-03-13','001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010001110001111100010010010010','https://images.unsplash.com/reserve/RFDKkrvXSHqBaWMMl4W5_Heavy%20company%20by%20Alessandro%20Desantis%20-%20Downloaded%20from%20500px_jpg.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80'),(5,'ahahahah','ahahaha','2019-03-13','ahahahahpopopopopo','https://images.unsplash.com/photo-1473603477862-9d352d4615e1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80'),(7,'Article Louis','Louis','2019-03-13','C\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour LouisC\'est un article incroyable de Louis fait par Louis, pour Louis','https://images.unsplash.com/photo-1425082661705-1834bfd09dca?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=755&q=80'),(8,'Article de Rapha','Rapha','2019-03-13','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque urna ante, lacinia ut sagittis nec, mattis vitae tortor. Suspendisse ut lacus in leo posuere porta in aliquam lacus. Phasellus nulla velit, vulputate a ex a, fermentum luctus est. Integer turpis libero, eleifend ut massa quis, mollis sodales sem. Nunc commodo fermentum tincidunt. Curabitur vehicula elit orci, quis imperdiet lorem vehicula eget. Vestibulum euismod lacus ut eros fringilla, ac blandit enim hendrerit. Curabitur vel nibh velit. Sed viverra dui vel urna blandit dictum. Nunc in sagittis sapien. Praesent lacinia lobortis est, vel imperdiet dolor luctus quis.\r\n\r\nPellentesque non ullamcorper nisl, quis ullamcorper purus. Proin vehicula arcu metus, in vestibulum purus fermentum vel. Aenean eget elit lacus. Vestibulum consequat, lorem a luctus gravida, sapien mi fringilla arcu, ut finibus leo neque et tortor. Donec non pellentesque nunc. Ut aliquam nisl vitae orci congue, ac ultrices lorem porttitor. Phasellus sed rhoncus turpis. Morbi dignissim tincidunt commodo. Nulla iaculis volutpat odio, ut maximus purus volutpat condimentum. In iaculis lacus sed nisl ultrices, sit amet iaculis ligula sodales. Fusce ut condimentum massa. Aliquam vulputate convallis bibendum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In non lacus dapibus lorem iaculis dignissim vitae varius dui.\r\n\r\nPhasellus imperdiet a orci vitae egestas. Vivamus varius justo neque, sed dapibus nisi tincidunt eu. Aliquam in ante in sapien finibus bibendum eu nec nibh. Duis ac feugiat odio, at tristique augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam tristique leo sem, sit amet rhoncus ipsum tincidunt vel. Phasellus bibendum ornare leo at aliquet. Vestibulum vulputate accumsan tempus. Donec feugiat nisi id pretium rutrum. Nam at arcu porta enim imperdiet suscipit eu at risus. Nulla eu odio nec sem dignissim porta. Vestibulum quis commodo justo.\r\n\r\nMorbi lacinia felis in sapien euismod egestas. In hac habitasse platea dictumst. Integer orci turpis, rutrum et molestie non, iaculis ac libero. Ut sit amet orci pulvinar, auctor nisi vitae, lobortis metus. Mauris tincidunt augue ante, quis elementum urna gravida et. Nam vulputate molestie nisl et lacinia. Ut tristique nisl eget tincidunt dapibus. Nulla vitae blandit libero. Sed commodo tempor posuere.\r\n\r\nQuisque ut gravida ante. Donec maximus ipsum non orci tristique, in pulvinar nunc venenatis. Etiam dapibus magna in velit cursus accumsan. Sed at condimentum quam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin tincidunt lacinia quam, eget porta ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus pretium dignissim justo, id scelerisque magna eleifend porttitor. Curabitur in egestas nulla. Morbi mollis egestas ligula non condimentum.','https://images.unsplash.com/photo-1503066211613-c17ebc9daef0?ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentaires` (
  `commentaires_id` int(11) NOT NULL AUTO_INCREMENT,
  `commentaires_pseudo` varchar(45) DEFAULT NULL,
  `commentaires_date` date DEFAULT NULL,
  `commentaires_contenu` text DEFAULT NULL,
  `id_article` int(11) DEFAULT NULL,
  PRIMARY KEY (`commentaires_id`),
  KEY `fk_commentaires_1_idx` (`id_article`),
  CONSTRAINT `fk_commentaires_1` FOREIGN KEY (`id_article`) REFERENCES `article` (`id_article`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaires`
--

LOCK TABLES `commentaires` WRITE;
/*!40000 ALTER TABLE `commentaires` DISABLE KEYS */;
INSERT INTO `commentaires` VALUES (1,'Toto','2019-03-12','hahahahaha',NULL),(4,'loulou','2019-03-12','tactac',7),(5,'benoit','2019-03-12','je t\'entends pas t\'es a ma gauche',7),(9,'Kal','2019-03-14','It\'s awesome!!!',7);
/*!40000 ALTER TABLE `commentaires` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-09 14:43:05
